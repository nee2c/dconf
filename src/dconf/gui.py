"""
This is a GUI for DConf
"""
import json
import tkinter as tk
import tkinter.filedialog as fd

from .dconf import getauth, userpass


class DConfGUI:
    """
    This is the DConf GUI
    """

    def __init__(self, root=None):
        """
        This is the init function
        """
        self.root = root or tk.Tk()
        self.root.title("DConf")
        self.root.geometry("800x600")
        self.root.minsize(800, 400)
        self.col = self.column()
        self.rad, self.base = self.baseraido()
        self.input = self.in_()
        self.output, self.butcopy, self.butsave = self.out()
        self.root.grid_rowconfigure(0, weight=1)
        self.root.grid_columnconfigure(0, weight=1)
        self.root.grid_columnconfigure(1, weight=3)

    def run(self):
        self.update()
        self.root.mainloop()

    def column(self):
        """
        This sets up the column frames
        """
        ops = tk.LabelFrame(
            self.root,
            text="Options",
        )
        out = tk.LabelFrame(
            self.root,
            text="Output",
        )
        ops.grid(row=0, column=0, sticky="nwes", padx=5, pady=5)
        out.grid(row=0, column=1, sticky="nwes", padx=5, pady=5)
        ops.columnconfigure(0, weight=1)
        ops.grid_propagate(False)
        out.grid_propagate(False)
        return [ops, out]

    def baseraido(self):
        """
        This is the base64 radio button
        """
        var = tk.StringVar()
        plan = tk.Radiobutton(self.col[0], text="Plan Text", value="plan", variable=var)
        b64 = tk.Radiobutton(self.col[0], text="base64", value="b64", variable=var)
        plan.select()
        plan.grid(row=0, column=0, sticky="nw")
        b64.grid(row=1, column=0, sticky="nw")
        return {"plan": plan, "b64": b64}, var

    def in_(self):
        """
        Create the input text boxes
        """
        in_ = {}
        for index, key in enumerate(["registry", "username", "password", "email"], 2):
            vals = {}
            vals["frame"] = tk.LabelFrame(self.col[0], text=key)
            vals["frame"].grid(row=index, column=0, sticky="nwes")
            vals["entry"] = tk.Entry(
                vals["frame"],
            )
            vals["entry"].pack(fill="both", expand=True)
            in_[key] = vals

        cbut = tk.Button(self.col[0], text="Clear", command=self.clear)
        cbut.grid(row=6, column=0, sticky="nwse")
        in_["clear"] = cbut
        return in_

    def out(self):
        """
        This is the text box
        """
        text = tk.Label(
            self.col[1],
        )
        text.grid(row=0, column=0, sticky="nw")
        bframe = tk.Frame(self.col[1])
        bframe.grid(row=1, column=0, sticky="nwes")
        bframe.grid_columnconfigure(0, weight=1)
        bframe.grid_columnconfigure(1, weight=1)
        bframe.grid_rowconfigure(0, weight=1)
        cbut = tk.Button(bframe, text="Copy", command=self.copy)
        cbut.grid(
            row=0,
            column=0,
        )
        sbut = tk.Button(
            bframe,
            text="Save",
            command=self.save,
        )
        sbut.grid(
            row=0,
            column=1,
        )
        self.col[1].grid_rowconfigure(0, weight=1)
        self.col[1].grid_rowconfigure(1, weight=0)
        self.col[1].grid_columnconfigure(0, weight=1)
        return text, cbut, sbut

    def copy(self):
        """
        This copies the text to the clipboard
        """
        self.root.clipboard_clear()
        self.root.clipboard_append(self.output.cget("text"))

    def clear(self):
        """
        This clears the text box
        """
        for val in self.input.values():
            if isinstance(val, dict):
                val["entry"].delete(0, "end")

    def update(self):
        """
        This updates the output text box
        """
        reg = self.input["registry"]["entry"].get()
        user = self.input["username"]["entry"].get()
        password = self.input["password"]["entry"].get()
        email = self.input["email"]["entry"].get()
        auth = {"auths": {reg: getauth(user, password) if self.base.get() == "b64" else userpass(user, password)}}
        if email:
            auth["auths"][reg]["email"] = email
        self.output.config(text=json.dumps(auth, indent=4))
        self.root.after(33, self.update)

    def save(self):
        """
        This saves the output to a file
        """
        path = fd.asksaveasfilename(
            defaultextension=".json",
            filetypes=[("JSON", "*.json")],
            initialfile="auth.json",
            parent=self.root,
            title="Save Auth",
        )
        if path:
            with open(path, "w") as f:
                f.write(self.output.cget("text"))


def main():
    """
    This is the main function
    """
    app = DConfGUI()
    app.run()


if __name__ == "__main__":
    main()
