"""
=====
dconf
=====

This module contains the main function for the CLI of dconf.

The main purpose of this program is to generate the docker client config.json file.

The config.json file is used to store the credentials for docker registries.
"""
import argparse
import base64
import getpass
import json
import logging

from dconf import __version__

log = logging.getLogger(__name__)


def parse_args(args=None):
    """
    Generates the command line arguments.

    :param args: The command line arguments.
    :type args: list
    :return: The parsed command line arguments.
    :rtype: argparse.Namespace
    """
    parser = argparse.ArgumentParser(
        description="dconf - A CLI for managing Docker containers",
    )

    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=f"dconf: {__version__}",
        help="Show the version and exit.",
    )
    parser.add_argument(
        "-l",
        "--log",
        action="store",
        default="ERROR",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="Set the logging level. Defaults to ERROR.",
    )
    parser.add_argument(
        "-b",
        "--base64",
        action="store_true",
        help="Base64 encode the username and password.",
        dest="base64",
    )
    parser.add_argument(
        "-p",
        "--password",
        action="store",
        help="accepts a password via the command line.",
        default=None,
        dest="password",
        type=str,
    )
    parser.add_argument(
        "-e",
        "--email",
        action="store",
        help="The email address to login with.",
        default=None,
        dest="email",
        type=str,
    )
    parser.add_argument(
        "-o",
        "--output",
        action="store",
        help="Save the credentials to a file.  Defaults to stdout.",
        default=None,
        dest="output",
    )

    parser.add_argument(
        "registry",
        action="store",
        help="The registry to login to.",
        type=str,
    )
    parser.add_argument(
        "username",
        action="store",
        help="The username to login with. Defaults to the current user.",
        nargs="?",
        default=getpass.getuser(),
        type=str,
    )

    return parser.parse_args(args)


def getauth(user, password):
    """
    Generates a dict with the base64 encoded username and password.

    :param user: The username to login with.
    :type user: str
    :param password: The password to login with.
    :type password: str
    :return: The auth dict.
    :rtype: dict
    """
    log.debug("Generating auth dict with base64 encoded username and password")
    return {"auth": base64.b64encode(f"{user}:{password}".encode("utf-8")).decode("utf-8")}


def userpass(user, password):
    """
    Generates a dict with the username and password.

    :param user: The username to login with.
    :type user: str
    :param password: The password to login with.
    :type password: str
    :return: The auth dict.
    :rtype: dict
    """
    log.debug("Generating auth dict with username and password")
    return {"username": user, "password": password}


def main(args=None):
    """
    The main function.  This is the entry point for the CLI.
    This will create the dict and either print it to stdout or write it to a file.

    :param args: The command line arguments.
    :type args: list
    """
    args = parse_args(args=args)

    log.setLevel(args.log)
    logging.basicConfig(level=args.log)
    log.debug("Set log level to %s", args.log)

    reg = args.registry
    log.debug("Registry: %s", reg)
    user = args.username
    log.debug("Username: %s", user)
    email = args.email
    log.debug("Email: %s", email)

    if args.password:
        password = args.password
        log.debug("Password passed via command line.")
    else:
        log.debug("Password not passed via command line.  Getting password from user.")
        password = getpass.getpass("Enter password: ")

    log.debug("Genarating dict.")
    auth = {"auths": {reg: getauth(user, password) if args.base64 else userpass(user, password)}}

    if email:
        log.debug("Adding email to auth dict.")
        auth["auths"][reg]["email"] = email

    if args.output:
        log.debug("Writing auth dict to file.")
        with open(args.output, "w") as f:
            json.dump(auth, f)
    else:
        log.debug("Printing auth dict to stdout.")
        print(json.dumps(auth))


if __name__ == "__main__":
    main()
