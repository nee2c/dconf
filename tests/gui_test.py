import json
import tkinter as tk

import pytest

from dconf.gui import DConfGUI, fd, main


@pytest.fixture
def gui():
    root = tk.Tk()
    gui = DConfGUI(root)
    # thread = Thread(target=gui.run)
    # thread.start()
    # gui.run()
    yield gui
    root.destroy()
    # thread.join()


def test_gui(gui):
    """
    Test all default values
    """
    assert gui.root.title() == "DConf"
    assert gui.root.minsize() == (800, 400)
    assert gui.col[0].cget("text") == "Options"
    assert gui.col[1].cget("text") == "Output"
    assert gui.base.get() == "plan"
    assert gui.rad["plan"].cget("text") == "Plan Text"
    assert gui.rad["b64"].cget("text") == "base64"
    for key in gui.input:
        if key == "clear":
            assert gui.input[key].cget("text") == "Clear"
            continue
        assert gui.input[key]["frame"].cget("text") == key
        assert gui.input[key]["entry"].get() == ""
    assert gui.output.cget("text") == ""
    assert gui.butcopy.cget("text") == "Copy"
    assert gui.butsave.cget("text") == "Save"


@pytest.mark.parametrize("rad", ["plan", "b64"])
@pytest.mark.parametrize("reg,user,pw,email", [("", "", "", ""), ("a", "b", "c", ""), ("a", "b", "c", "d")])
def test_gui_input(gui, rad, reg, user, pw, email):
    """
    Test the input
    """
    if rad == "plan":
        gui.rad["plan"].select()
    elif rad == "b64":
        gui.rad["b64"].select()
    else:
        raise ValueError("rad must be plan or b64")

    gui.input["registry"]["entry"].insert(0, reg)
    gui.input["username"]["entry"].insert(0, user)
    gui.input["password"]["entry"].insert(0, pw)
    gui.input["email"]["entry"].insert(0, email)

    gui.update()
    test = json.loads(gui.output.cget("text"))
    assert test

    if rad == "plan":
        for key in test["auths"]:
            assert key == reg
            assert test["auths"][key]["username"] == user
            assert test["auths"][key]["password"] == pw
            if email:
                assert test["auths"][key]["email"] == email
    elif rad == "b64":
        for key in test["auths"]:
            assert key == reg
            if user and pw:
                assert test["auths"][key]["auth"] == "Yjpj"
            if email:
                assert test["auths"][key]["email"] == email


@pytest.mark.parametrize("but", [True, False])
def test_gui_clear(gui, but):
    """
    Test the clear button
    """
    gui.input["registry"]["entry"].insert(0, "a")
    gui.input["username"]["entry"].insert(0, "b")
    gui.input["password"]["entry"].insert(0, "c")
    gui.input["email"]["entry"].insert(0, "d")

    if but:
        gui.input["clear"].invoke()
    else:
        gui.clear()

    assert gui.input["registry"]["entry"].get() == ""
    assert gui.input["username"]["entry"].get() == ""
    assert gui.input["password"]["entry"].get() == ""
    assert gui.input["email"]["entry"].get() == ""


@pytest.mark.parametrize("but", [True, False])
def test_gui_copy(gui, but):
    """
    Test the copy button
    """
    gui.output.config(text="test")
    if but:
        gui.butcopy.invoke()
    else:
        gui.copy
    assert gui.root.clipboard_get() == "test"


@pytest.mark.parametrize("but", [True, False])
def test_gui_save(gui, temp_file, monkeypatch, but):
    """
    Test the save button
    """
    monkeypatch.setattr(fd, "asksaveasfilename", lambda *args, **kwargs: str(temp_file))
    gui.output.config(text="test")
    if but:
        gui.butsave.invoke()
    else:
        gui.save()
    with open(temp_file, "r") as f:
        assert f.read() == "test"


def test_save_cancel(gui, monkeypatch):
    """
    Test the save button and cancel the asksaveasfilename dialog
    """
    monkeypatch.setattr(fd, "asksaveasfilename", lambda *args, **kwargs: "")
    gui.butsave.invoke()


def test_gui_run(gui, monkeypatch):
    """
    Test the run function
    """
    monkeypatch.setattr(gui.root, "mainloop", lambda *args, **kwargs: None)
    gui.run()


def test_gui_main(gui, monkeypatch):
    """
    Test the main function
    """
    monkeypatch.setattr(DConfGUI, "run", lambda *args, **kwargs: None)
    main()
