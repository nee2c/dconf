import os

import pytest


@pytest.fixture
def temp_file(tmp_path):
    # Create a temporary file
    temp_file = tmp_path / "temp.json"
    yield str(temp_file)

    # Delete the file after the test
    if os.path.isfile(temp_file):
        os.remove(temp_file)
