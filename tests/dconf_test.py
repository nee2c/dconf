import json

import pytest

import dconf.dconf


def test_noargs(capsys):
    """
    Run the script with no arguments.
    Should exit with a non-zero code.
    Check the output to stderr.
    """
    with pytest.raises(SystemExit) as err:
        dconf.dconf.main()

    assert err.type == SystemExit
    assert err.value.code == 2

    err = capsys.readouterr().err

    assert err.startswith("usage:")


@pytest.mark.parametrize("args", ["-h", "--help"])
def test_help(capsys, args):
    """
    Run the script with the help flag.
    Should exit with a zero code.
    Check the output to stdout.
    """
    with pytest.raises(SystemExit) as err:
        dconf.dconf.main([args])

    assert err.type == SystemExit
    assert err.value.code == 0

    out = capsys.readouterr().out

    assert out.startswith("usage:")


@pytest.mark.parametrize("args", ["-v", "--version"])
def test_version(capsys, args):
    """
    Run the script with the version flag.
    Should exit with a zero code.
    Check the output to stdout.
    """
    with pytest.raises(SystemExit) as err:
        dconf.dconf.main([args])

    assert err.type == SystemExit
    assert err.value.code == 0

    out = capsys.readouterr().out

    assert out == f"dconf: {dconf.dconf.__version__}\n"


@pytest.mark.parametrize("targs", ["-l", "--log"])
@pytest.mark.parametrize("log_level", ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"])
def test_log(targs, log_level):
    """
    Makes sure the log level argument are paseed correctly.
    """
    args = dconf.dconf.parse_args([targs, log_level, "a", "b", "-p", "c"])
    assert args.log == log_level


@pytest.mark.parametrize("base", [None, "-b", "--base64"])
@pytest.mark.parametrize("email", [None, "-e", "--email"])
@pytest.mark.parametrize("output", [None, "-o", "--output"])
def test_email(capsys, temp_file, base, email, output):
    """
    Makes sure the email argument is passed correctly.
    """
    args = ["a", "b", "-p", "c"]

    if base:
        args.append(base)

    if email:
        args.append(email)
        args.append("test@example.com")

    if output:
        args.append(output)
        args.append(temp_file)

    dconf.dconf.main(args)

    if output:
        with open(temp_file, "r") as f:
            data = json.load(f)

        assert data

    else:
        data = json.loads(capsys.readouterr().out)
        assert data

    if email:
        assert data["auths"]["a"]["email"] == "test@example.com"

    if base:
        assert data["auths"]["a"]["auth"] == "Yjpj"
    else:
        assert data["auths"]["a"]["username"] == "b"
        assert data["auths"]["a"]["password"] == "c"


def test_passwdcap(capsys, monkeypatch):
    """
    Makes sure the password can be passed correctly from stdin.
    """
    args = [
        "a",
        "b",
    ]

    monkeypatch.setattr(dconf.dconf.getpass, "getpass", lambda x: "c")

    dconf.dconf.main(args)

    data = json.loads(capsys.readouterr().out)

    assert data["auths"]["a"]["username"] == "b"
    assert data["auths"]["a"]["password"] == "c"
