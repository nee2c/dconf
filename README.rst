.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

.. image:: https://gitlab.com/nee2c/dconf/badges/master/pipeline.svg

.. image:: https://readthedocs.org/projects/dconf/badge/?version=latest
    :target: https://dconf.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

|

=====
dconf
=====


This is a simple cli tool to generated docker config file to authorize docker to pull from a private registry.


Installation
============

To install dconf you can install from
`gitlab package registry <https://gitlab.com/nee2c/dconf/-/packages/>`_


.. _sim: https://gitlab.com/nee2c/dconf/-/blob/master/src/dconf/sim.py
.. _ui: https://gitlab.com/nee2c/dconf/-/blob/master/src/dconf/ui.py

.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
